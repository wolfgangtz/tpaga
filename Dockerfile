FROM python:3.6.5-jessie

WORKDIR /
COPY . /app

ENV ENV local
EXPOSE 8443

RUN apt update -y
RUN apt install stunnel -y
RUN chmod +x app/runserver

RUN pip install -r /app/requirements.txt

WORKDIR /app

CMD ["sh", "runserver"]



