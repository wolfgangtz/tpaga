# Introducción

El presente proyecto tiene como objetivo dar solución a la prueba tecnica suegerida por TPaga para optar por el puesto de backed developer dentro de la organización.

La prueba consistia en crear un minicomercio, el cual, cuenta con un unico arcticulo/servicio el cual sera vendido con ayuda de la API de TPaga, dicha labor tiene subtareas, las cules basicamente son:

- Llevar una traza de las compras realizadas mediante el sitio.
- Tener la posibilidad de revertir una compra mediante un usuario administrador.
- Implementar autenticación para los distintos usuarios que pueden convivir en la plataforma.
- Desplegar el servicio en algún servidor o dockerizarlo para faciliar el proceso de revisión


## Tiempo requerido para la prueba
El proceso de desarrollo de la prueba, tomo un tiempo de desarrollo de 11 Horas, en el cual se desarrollaron las siguientes características:

- Registro de un usuario en la plataforma
- login de un usuario en la plataforma
- Espacio de "mi-cuenta" donde el usuario podrá ver su historico de compras
- Adecuacion del administrador de Django para que desde hay se puedan revertir las transacciones haciendo uso de las Actions
- Dockerización del servicio
- Implementación de Stunnel para poder utilizar HTTPS desde un entorno local y de pruebas
- Desarrollo de una interfaz amigable para el usuario a partir de un template

cabe destacar que todos los formularios tienen validaciones.

## Usuarios a utilizar en el sistema

Debido a que el mismo proyecto permite crear usuarios, no se dejo uno por defecto para probar la plataforma, de esta manera, se puede hacer un ejercicio completo mediante ella.
por otro lado, si se creo un usuario administrador con el cual se podra ingresar al administrador de Django, las credenciales son:

```bash
user: admin@tpaga.co
password: QWERTY123456
```

el usuario administrador anteriormente dicho se crea al correr las migraciones, y no es necesario crearlo manualmente.

## Preparación del proyecto para su prueba

Para probar el proyecto, no es mas que pararse en la raiz, en donde debe de estar el Dockerfile y ejecutar el comando:

```bash
docker build -t tpaga/test .
```
 paso seguido, correr la imagen:
```bash
docker run --rm -p 8443:8443 --name tpaga-test -it  tpaga/test
```

cabe destacar que el proyecto fue previsto para correr en el puerto 8443. Una vez levantada la imagen, podrá acceder a la pantalla inicial en la siguiente dirección:
```bash
https://0.0.0.0:8443/minicomercio/
```

por otro lado, para entrar al administrador de Django, se puede hacer por aqui:
```bash
https://0.0.0.0:8443/admin/
```
Hay que tener en cuenta, que si se quiere probar el proyecto desde un móvil, es necesario saber la IP del computador donde esta corriendo el proyecto con la ayuda del comando:
```bash
ifconfig
```
cuando tengamos la IP, solo basta con reemplazarla en las URL de la siguiente manera:
```bash
https://<IP>:8443/admin/
```

es de vital importancia, que el dispositivo móvil y el computador donde este el proyecto corriendo, se encuentren en la misma red WIFI.
## Mejoras

Debido a que el sistema se construyo para una prueba, no se pudieron realizar todas las cosas que hubiesen dado una mejor experiencia de usuario pero que debido al tiempo. no lograron realizarse, estas serian:

- Capturar peticiones 5XX y 4XX y redireccionarlas a un template de error.
- Montar el proyecto en un servidor dweb mas robusto como NGINX.
- implementar pruebas unitarias
- realizar mas validaciones en tiempo real como por ejemplo, utilizar Celery para que de manera periodica se verificara el estado de las compras que tengan un estado pendiente

