from django.contrib import admin
from django.contrib import messages
from minicomercio.models.productos import Producto
from minicomercio.models.ventas import Venta
from minicomercio.views.helpers.tpaga_API import revertir_compra



class VentasAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'producto', 'amount', 'status', 'date_created',)
    actions = ['revert_payment']

    def revert_payment(self, request, queryset):
        print(queryset)
        errors = []
        for obj in queryset:
            if obj.status != "REVERTED":
                if not revertir_compra(obj):
                    errors.append(obj)
        if len(errors) > 0:
            self.message_user(request, str(len(errors)) + ' sales cannot be reverted.', messages.ERROR)
        self.message_user(request, 'Selected sales was reverted', messages.INFO)

    revert_payment.short_description = "Revert the payment of this article"


admin.site.register(Producto)
admin.site.register(Venta,VentasAdmin)