from django.apps import AppConfig


class MinicomercioConfig(AppConfig):
    name = 'minicomercio'
