from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.core.validators import validate_email


class AmountForm(forms.Form):
    amount = forms.IntegerField(
        required=True,
        label='Cantidad',
        widget=forms.NumberInput(
            attrs={
                'style' : 'color: black;',
                'type':'number'
            }
        )
    )
    def clean(self):
        cleaned_data = super(AmountForm, self).clean()
        amount = cleaned_data.get('amount')

        try:
            int(amount)
        except Exception as e:
            raise forms.ValidationError("Invalid input data.")
        return cleaned_data

class LogInUserForm(forms.Form):
    email = forms.CharField(
        required=True,
        label='Correo Electrónico',
        max_length=100,
        validators=[validate_email]

    )
    password = forms.CharField(
        required=True,
        label='Contraseña',
        max_length=100,
        widget=forms.PasswordInput
    )
    def clean(self):
        cleaned_data = super(LogInUserForm, self).clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        if email is not None and password is not None:
            user = authenticate(
                username=email,
                password=password
            )
            if not user:
                raise forms.ValidationError("Invalid Credentials.")

        return cleaned_data

class RegisterUserForm(forms.Form):
    name = forms.CharField(
        required=True,
        label='Nombre',
        max_length=100
    )

    last_name = forms.CharField(
        required=True,
        label='Apellidos',
        max_length=100
    )

    email = forms.CharField(
        required=True,
        label='Correo Electrónico',
        max_length=100,
        validators=[validate_email]

    )
    password = forms.CharField(
        required=True,
        label='Contraseña',
        max_length=100,
        widget=forms.PasswordInput
    )

    validate_password = forms.CharField(
        required=True,
        label='Validar contraseña',
        max_length=100,
        widget=forms.PasswordInput
    )

    def clean(self):
        User = get_user_model()
        cleaned_data = super(RegisterUserForm, self).clean()

        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('validate_password')

        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError("The two password fields must match.")

        email = cleaned_data.get('email')

        user = User.objects.filter(email=email)
        if user:
            raise forms.ValidationError("This email is in use now.")

        return cleaned_data
