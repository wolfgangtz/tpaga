import uuid
from django.db import models
from minicomercio.models.productos import Producto
from django.contrib.auth import get_user_model

class Venta(models.Model):

    User = get_user_model()

    PENDING = 'PENDING'
    APPROVED = 'APPROVED'
    DELIVERED = 'DELIVERED'
    REVERTED = 'REVERTED'
    FAILED = 'FAILED'
    EXPIRED = 'EXPIRED'

    STATUS_TYPES = (
        (PENDING, PENDING),
        (APPROVED, APPROVED),
        (DELIVERED, DELIVERED),
        (REVERTED, REVERTED),
        (FAILED, FAILED),
        (EXPIRED, EXPIRED)

    )


    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )

    amount = models.IntegerField(
        null=False,
        blank=False
    )

    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        blank=False
    )

    idempotency_token = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True
    )

    producto = models.ForeignKey(
        Producto,
        on_delete=models.PROTECT,
        blank=False
    )

    status = models.CharField(
        max_length=30,
        choices=STATUS_TYPES,
        null=False,
        blank=False
    )

    token_tpaga = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

    date_created = models.DateTimeField(
        auto_now_add=True
    )

    date_modified = models.DateTimeField(
        auto_now=True
    )