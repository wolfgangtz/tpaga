from django.urls import path
from minicomercio.views import mi_cuenta
from minicomercio.views import tpaga
from minicomercio.views import auth
from minicomercio.views import registro

urlpatterns = [
    path(
        '',
        mi_cuenta.index,
        name='index'
    ),
    path(
        'registrarse/',
        registro.registrarse,
        name='registrarse'
    ),
    path(
        'registrar-usuario/',
        registro.registrar_usuario,
        name='registrar-usuario'
    ),
    path(
        'ingresar/',
        auth.ingresar,
        name='ingresar'
    ),
     path(
        'validar-usuario/',
        auth.validar_usuario,
        name='validar-usuario'
    ),
    path(
        'mi-cuenta/',
        mi_cuenta.mi_cuenta_user,
        name='mi-cuenta'
    ),
    path(
        'comprar/',
        tpaga.comprar,
        name='comprar'
    ),
    path(
        'detalle-compra/',
        tpaga.detalle_compra,
        name='detalle_compra'
    ),
    path(
        'logout/',
        auth.logout_user,
        name='logout'
    ),
    path(
        'error/',
        tpaga.error,
        name='error'
    ),
    path(
        'exito/',
        tpaga.exito,
        name='exito'
    ),
]
