from django.shortcuts import render
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from minicomercio.forms import LogInUserForm
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from django.contrib.auth import logout

Users = get_user_model()

def ingresar(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/mi-cuenta/')

    return render(
        request,
        'minicomercio/ingresar.html',
        { 'form': LogInUserForm()}
    )

def validar_usuario(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/mi-cuenta/')

    form = LogInUserForm(request.POST)

    if not form.is_valid():
        return render(
            request,
            'minicomercio/ingresar.html',
            {'form': form}
        )

    login(
        request,
        Users.objects.get(email=form.cleaned_data['email'])
    )

    return HttpResponseRedirect('/minicomercio/mi-cuenta/')

def logout_user(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/')
    logout(request)
    return HttpResponseRedirect('/minicomercio/')
