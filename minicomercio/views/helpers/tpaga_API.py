import json
import uuid
import base64
import requests
import datetime
from rest_framework import status
from django.conf import settings
from minicomercio.models.ventas import Venta
from minicomercio.models.productos import Producto
from minicomercio.views.resources.endpoints import ORDEN_DE_COMPRA_URL
from minicomercio.views.resources.endpoints import CONSULTA_ESTADO_COMPRA
from minicomercio.views.resources.endpoints import REVERTIR_PAGO_URL

def generate_tpaga_token():
    token = base64.b64encode((settings.USER_TPAGA + ":" + settings.PASSWORD_TPAGA).encode('utf-8')).decode('utf-8')
    return "Basic " + token

def generate_idempotency_token():
    flag = True
    uuid_token = None

    while flag:
        uuid_token_generated = str(uuid.uuid4())
        if not Venta.objects.filter(idempotency_token=uuid_token_generated):
            uuid_token = uuid_token_generated
            flag = False

    return uuid_token

def comprador_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def format_response_to_json(response):
    """
    Format Http response to JSON
    """
    response = response.content if hasattr(response, 'content') else response
    content_response = None
    try:
        json_response = None
        if type(response) == bytes:
            json_response = json.loads(response.decode('utf-8'))
        else:
            json_response = json.loads(response)
        content_response = {
            'status': 'ok',
            'result': json_response
        }
    except Exception as e:
        content_response = {
            'status': 'err',
            'result': str(e)
        }

    return content_response


def revertir_compra(venta):
    result_request = False
    REQUEST_DATA = {
        "payment_request_token": venta.token_tpaga
    }

    request = requests.post(
        REVERTIR_PAGO_URL,
        headers={
            "Authorization": generate_tpaga_token(),
            "Content-Type": "application/json"
        },
        json=REQUEST_DATA
    )
    response = format_response_to_json(request)
    if response['status'] == 'ok':
        if response['result']['status'] == 'reverted':
            venta.status = 'REVERTED'
            venta.save()
            result_request = True

    return result_request


def orden_de_compra(request,cantidad, user):

    solicitud_venta = Venta.objects.create(
        user=user,
        idempotency_token=generate_idempotency_token(),
        producto=Producto.objects.first(),
        status='PENDING',
        amount=int(cantidad)
    )
    solicitud_venta.save()

    expire_data = datetime.datetime.now() + datetime.timedelta(days=settings.DAYS_EXPIRE_PURCHASE_ORDER)
    expire_data = expire_data.strftime('%Y-%m-%dT%H:%M:%S.%f%z')

    cost = cantidad * 1500000

    REQUEST_DATA = {
        "cost": cost,
        "purchase_details_url":request.build_absolute_uri("/minicomercio/exito/").replace('http://','https://'),
        "idempotency_token": str(solicitud_venta.idempotency_token),
        "order_id": str(solicitud_venta.id),
        "terminal_id": "sede_norte",
        "purchase_description": "Compra de PS4 en tienda online, retirar en sede norte",
        "purchase_items": [
            {
                "name": "Play Station 4",
                "value": str(cantidad) + " Unidades compradas"
            }
        ],
        "user_ip_address": comprador_ip(request),
        "expires_at": expire_data
    }

    request = requests.post(
        ORDEN_DE_COMPRA_URL,
        headers={
            "Authorization": generate_tpaga_token(),
            "Content-Type": "application/json"
        },
        json=REQUEST_DATA
    )
    return (format_response_to_json(request), request.status_code, solicitud_venta)


def obtener_estado_actual(compra):
    url = CONSULTA_ESTADO_COMPRA
    url = url.replace('<TOKEN-TEPAGA>', compra.token_tpaga)
    request = requests.get(
        url,
        headers={
            "Authorization": generate_tpaga_token(),
            "Content-Type": "application/json"
        }
    )
    response = format_response_to_json(request)
    print(response)
    if response['status'] != 'ok':
        return "UNKNOWN"

    status_received = response['result']['status'] if 'status' in response['result'] else response['result']['data']['status']
    if status_received == 'paid':
        compra.status = 'APPROVED'
        compra.save()
    elif status_received == 'delivered':
        compra.status = 'DELIVERED'
        compra.save()
    elif status_received == 'reverted':
        compra.status = 'REVERTED'
        compra.save()
    elif status_received == 'failed':
        compra.status = 'FAILED'
        compra.save()
    elif status_received == 'created':
        compra.status = 'PENDING'
        compra.save()
    elif status_received == 'expired':
        compra.status = 'EXPIRED'
        compra.save()


    return compra.status

def consultar_compras(user):
    final_response = []
    compras = Venta.objects.filter(user=user)
    for cada_compra in compras:
        compra = {}
        compra['producto'] = cada_compra.producto.name
        compra['estado'] = obtener_estado_actual(cada_compra)
        compra['cantidad'] = cada_compra.amount
        compra['fecha_compra'] = cada_compra.date_created.strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        final_response.append(compra)
    return final_response
