from django.shortcuts import render
from django.http import HttpResponseRedirect
from minicomercio.forms import AmountForm
from minicomercio.views.helpers.tpaga_API import consultar_compras


def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/mi-cuenta/')

    return render(
        request,
        'minicomercio/index.html',
        {}
    )

def mi_cuenta_user(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/')

    return render(
        request,
        'minicomercio/mi-cuenta.html',
        {'form': AmountForm(), 'compras':consultar_compras(request.user)}
    )
