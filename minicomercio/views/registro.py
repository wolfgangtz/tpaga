from django.shortcuts import render
from django.http import HttpResponseRedirect
from minicomercio.forms import RegisterUserForm
from django.contrib.auth import get_user_model
from django.contrib.auth import login


Users = get_user_model()

def registrarse(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/mi-cuenta/')

    return render(
        request,
        'minicomercio/registrarse.html',
        {'form': RegisterUserForm()}
    )

def registrar_usuario(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/mi-cuenta/')

    form = RegisterUserForm(request.POST)

    if not form.is_valid():
        return render(
            request,
            'minicomercio/registrarse.html',
            {'form': form}
        )

    user_data = form.cleaned_data
    user = Users.objects.create_user(
        user_data['email'],
        user_data['email'],
        user_data['password']
    )
    user.first_name = user_data['name']
    user.last_name = user_data['last_name']
    user.save()

    login(
        request,
        user
    )

    return HttpResponseRedirect('/minicomercio/mi-cuenta/')