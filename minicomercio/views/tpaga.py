from django.shortcuts import render
from django.http import HttpResponseRedirect
from minicomercio.forms import AmountForm
from minicomercio.views.helpers.tpaga_API import orden_de_compra
from minicomercio.views.helpers.tpaga_API import consultar_compras
from rest_framework import status


def comprar(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/')
    form = AmountForm(request.POST)

    if not form.is_valid():
        return render(
            request,
            'minicomercio/mi-cuenta.html',
            {'form': form, 'compras':consultar_compras(request.user)}
        )

    compra_data = form.cleaned_data
    orden_de_compra_json, request_status, venta = orden_de_compra(request,compra_data['amount'],request.user)

    if not status.is_success(request_status):
        return HttpResponseRedirect('/minicomercio/error/')

    venta.token_tpaga = orden_de_compra_json['result']['token']
    venta.save()

    return HttpResponseRedirect(orden_de_compra_json['result']['tpaga_payment_url'])


def detalle_compra(request):
    return HttpResponseRedirect('/minicomercio/mi-cuenta/')




def error(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/')

    return render(
        request,
        'minicomercio/error.html',
        {}
    )

def exito(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/minicomercio/')

    return render(
        request,
        'minicomercio/exito.html',
        {}
    )